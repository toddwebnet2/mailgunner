<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MailgunResult extends Model
{
    protected $table = 'mailgun_results';
    protected $fillable = [
        'mailgun_id',
        'message_id',
        'event',
        'recipient',
        'status',
        'message',
        'event_date'
    ];

    public static function lastDate()
    {
        $maxDate = self::max('event_date');

        return $maxDate;
    }
}