<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailTemplate extends Mailable
{
    use Queueable, SerializesModels;

    protected $from;
    protected $subject;
    protected $body;
    public function __construct($from, $subject, $body)
    {
        $this->from = $from;
        $this->subject = $subject;
        $this->body = $body;
    }

    PUBLIC FUNCTION build(){

    }

}
