<?php

namespace App\Console\Commands;

use App\Models\Email;
use App\Services\MailgunnerService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendMail extends Command
{
    protected $signature = 'sendmail';

    //$mailGun = new MailgunnerService();
    //$mailGun->sendMail("jamesthetodd@gmail.com", "xrewards@experienciasxcaret.com.mx", "subject", " I want to move to linux");
    //$mailGun->sendMail("toddwebnet@gmail.com", "xrewards@experienciasxcaret.com.mx", "I try too", " I try too");


    public function handle()
    {
        Log::info('start sendmail');
        $mailGun = new MailgunnerService();
        do {
            $email = Email::where('sent', '0')->where('to', 'LIKE', '%@%')->first();
            if ($email !== null) {
                try {
                    $messageId = $mailGun->sendMail($email->to, $email->from, $email->subject, $email->body);
                    $email->sent = 1;
                    $email->message_id = $messageId;
                } catch (\Exception $e) {
                    $email->sent = 2;
                }

                $email->save();
            }
            print ".";
        } while ($email !== null);
        Log::info('end sendmail');
    }
}
