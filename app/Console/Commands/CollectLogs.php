<?php

namespace App\Console\Commands;


use App\Models\MailgunResult;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Mailgun\Mailgun;

class CollectLogs extends Command
{
    protected $signature = 'collectLogs';

    public function handle()
    {
        Log::info('start collectLogs');
        $domain = env('MAIL_DOMAIN');
        $secret = env('MAIL_SECRET');
        $mailgun = Mailgun::create($secret);
        $date = MailgunResult::lastDate();

        $params = [
            //    'limit' => 1
            // 'ascending' => 'yes',

        ];

        // $date = "2018-10-1";
        if ($date !== null) {
            $params['begin'] = date(\DateTime::RFC2822, strtotime($date));;
        }

        $response = $mailgun->events()->get($domain, $params);


        while (count($response->getItems()) > 0) {
            print ".";
            foreach ($response->getItems() as $item) {
                print "|";
                $message = $item->getMessage();
                $messageId = $message['headers']['message-id'];

                $data = [
                    'mailgun_id' => $item->getId(),
                    'message_id' => $messageId,
                    'event' => $item->getEvent(),
                    'recipient' => $item->getRecipient(),
                    'status' => json_encode($item->getDeliveryStatus()),
                    'message' => json_encode($message),
                    'event_date' => $item->getEventDate()->getTimestamp(),
                ];

                MailgunResult::create($data);
            }
            $response = $mailgun->events()->nextPage($response);
        }
        Log::info('start collectLogs');

        // $x = $mailgun->events()->lastPage($response);
        // print_r($x);
    }
}
