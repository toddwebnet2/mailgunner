<?php

namespace App\Console\Commands;

use App\Services\MailgunnerService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TestMail extends Command
{
    protected $signature = 'test:mail';

    public function handle()
    {
        Log::info('start test:mail');
        $mailGun = new MailgunnerService();
//        $mailGun->sendMail("jamesthetodd@gmail.com", "toddwebnet@gmail.com", "subject", " I want to move to linux");
        $mailGun->sendMail("toddwebnet@gmail.com", "toddwebnet@gmail.com", "I try too", " I try too");
        Log::info('end test:mail');
    }
}
