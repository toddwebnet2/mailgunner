<?php

namespace App\Services;

use Mailgun\Mailgun;

class MailgunnerService
{
    private $domain;
    private $mailgun;

    public function __construct()
    {
        // $this->domain = env('MAIL_DOMAIN');
        // $this->mailgun = Mailgun::create(env('MAIL_SECRET'));

    }

    public function sendMail($to, $from, $subject, $body)
    {
        $domain = env('MAIL_DOMAIN');
        $secret = env('MAIL_SECRET');
        $from = "noreply@mail.fei-email.com";
        // dd([$domain, $secret, $to, $from, $subject, $body]);
        $mailgun = Mailgun::create($secret);
        $feedback = $mailgun->messages()->send($domain, [
            'to' => $to,
            'from' => $from,
            'subject' => $subject,
            'html' => $body
        ]);

        return $feedback->getId();
    }

}
