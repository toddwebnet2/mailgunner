<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailgunResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailgun_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mailgun_id', 255);
            $table->string('message_id', 255);
            $table->string('event');
            $table->string('recipient', 255);
            $table->text('status');
            $table->text('message');
            $table->integer('event_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailgun_results');
    }
}
